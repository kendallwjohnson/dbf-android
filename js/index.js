/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

//Custom Stuff Added for DBF App
 var videos = {};
function getCalendarData()
{
    
    
     $.ajax(
    {
        url:'http://dbfchurch.com/calendar/feed/',
        type:'GET',
        dataType:'XML',
        error:function(jqXHR,text_status,strError)
            {
                alert("error");
            },
        timeout:60000,
        success:function(data)
            {
                //this looks funny, but I'm doing it this so I only get the app category ones.
                var items = $(data).find('event_category').parent().parent();
                $.each(items, function(index, value)
                       {
                            
                          var url =  '<img id="'+index+'" src="'+$(value).find('event_image').text()+'" />';
                          //$(url).prop('id', index);
                          var item = '<div>'+
                                                  //'<div class="m-card-light">'+
                                                  $(url).prop('outerHTML') +
                                                   // '<p class="m-caption">Caption #1</p>'+
                                                 // '</div>'+
                                               '</div>';
                              //update the above to also include data-info on it.
                              var info_array = [];
                              info_array.push({title: $(value).find('title').text(), where: $(value).find('venue').text(), startdate: $(value).find('startdate').text(), enddate: $(value).find('enddate').text(), desc: $(value).find('content').text(),
                                              venue_address: $(value).find('venue_address').text()+' | ' + $(value).find('venue_state').text() +' | ' + $(value).find('venue_zip').text(), organizer: $(value).find('organizer').text(), organizer_email: $(value).find('organizer_email').text(), organizer_phone: $(value).find('organizer_phone').text()});
                             $(".carousel").append($(item).prop('outerHTML'));
                             //now stash some stuff in the data attribute
                             $(".carousel #"+index).data("info", info_array);
                       });
                
                $('.carousel').slick(
                                       {
                                           dots:true,
                                           infinite: true,
                                           slidesToShow: 1,
                                           onAfterChange: function()
                                           {
                                               var dataInfo = $(".slick-active").children().data('info')[0];
   
                                               $("#titleDetail").text(dataInfo['title']);
                                               $("#startdate").text(dataInfo['startdate']);
                                               $("#enddate").text(dataInfo['enddate']);
                                               $("#whereDetail").text(dataInfo['where']);
                                               $("#detailDetail").text(dataInfo['desc']);
                                               $("#organizer").text(dataInfo['organizer']);
                                               $("#organizer_phone").text(dataInfo['organizer_phone']);
                                               $("#organizer_email").text(dataInfo['organizer_email']);
                                               $("#detailDiv").show();
                                           },
                                           onInit: function()
                                           {
                                               var dataInfo = $(".slick-active").children().data('info')[0];
                                               $("#titleDetail").text(dataInfo['title']);
                                               $("#startdate").text(dataInfo['startdate']);
                                               $("#enddate").text(dataInfo['enddate']);
                                               $("#whereDetail").text(dataInfo['where']);
                                               $("#detailDetail").text(dataInfo['desc']);
                                               $("#organizer").text(dataInfo['organizer']);
                                               $("#organizer_phone").text(dataInfo['organizer_phone']);
                                               $("#organizer_email").text(dataInfo['organizer_email']);
                                               $("#detailDiv").show();
                                           }
                                           });
            }
    });
}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

function getVideoData()
{
    var video1Complete = false;
    var video2Complete = false;
    var video3Complete = false;
    //make the call to get page 1
    $.ajax(
    {
        url:'http://vimeo.com/api/v2/album/2848113/videos.json?page=1',
        type:'GET',
        dataType:'json',
        error:function(jqXHR,text_status,strError)
            {
                alert("no connection to videos");
            },
        timeout:60000,
        success:function(data)
            {
                $(data).each(function(index, value)
                             {
                                videos[value['upload_date']] = value;
                             });
                video1Complete = true;
                
                if (video1Complete && video2Complete && video3Complete)
                {
                    createVideoHTML();
                }
            }
    });
    
    //make the call to get page 2
    $.ajax(
    {
        url:'http://vimeo.com/api/v2/album/2848113/videos.json?page=2',
        type:'GET',
        dataType:'json',
        error:function(jqXHR,text_status,strError)
            {
                alert("no connection to videos");
            },
        timeout:60000,
        success:function(data)
            {
                $(data).each(function(index, value)
                             {
                                videos[value['upload_date']] = value;
                             });
                video2Complete = true;
                
                 if (video1Complete && video2Complete && video3Complete)
                {
                    createVideoHTML();
                }
            }
    });
    
    //make the call to get page 3
    $.ajax(
    {
        url:'http://vimeo.com/api/v2/album/2848113/videos.json?page=3',
        type:'GET',
        dataType:'json',
        error:function(jqXHR,text_status,strError)
            {
                alert("no connection to videos");
            },
        timeout:60000,
        success:function(data)
            {
                $(data).each(function(index, value)
                             {
                                videos[value['upload_date']] = value;
                             });
                video3Complete = true;
                
                 if (video1Complete && video2Complete && video3Complete)
                {
                    createVideoHTML();
                }
            }
    });
}


function createVideoHTML()
{
    $.each(videos, function(index, value)
                   {
                        if (value['tags'].toLowerCase() == 'testimonies')
                        {
                            //now we need to append to the mediaListView element.
                            $("#testimoniesListView").append('<li><a style="font-size:1em" href="video.html?video_id='+value['id']+'"><img src="'+value['thumbnail_medium']+'" /><h1>'+value['title']+'</h1></a></li>');
                        }
                        else
                        {
                            //now we need to append to the mediaListView element.
                            $("#teachingsListView").append('<li><a style="font-size:1em" href="video.html?video_id='+value['id']+'"><img src="'+value['thumbnail_medium']+'" /><h1>'+value['title']+'</h1></a></li>');
                        }
                   });
    $("#testimoniesListView").listview('refresh');
    $("#teachingsListView").listview('refresh');
}

function change_media(type)
{
    if (type == 'teachings')
    {
        $("#teachingsListView").show()
        $("#testimoniesListView").hide();
    }
    else
    {
        $("#teachingsListView").hide()
        $("#testimoniesListView").show();
    }
}

function sendEmail()
{
    //just grab all of the values here
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var phone = $("#phone").val();
    var email = $("#email").val();
    var request = $("#request").val();
    var staff = $("#staff").prop('checked') ? '(STAFF ONLY)' : '';
    var groups =( $("#groups").prop('checked')) ? 'I am intersted in getting involved in a community group.' : '';
    var serving = ($("#serving").prop('checked')) ? 'I am interested in serving in the church.' : '';
     $.ajax(
    {
        type: "POST",
        url: "https://mandrillapp.com/api/1.0/messages/send.json",
        data: {
          'key': 'IfP71ggzpCbL0fDY9A2m1Q',
          'message': {
            'from_email': 'dbfconnections@gmail.com',
            'to': [
                {
                  'email': 'connection@discovertheone.com',
                  'name': 'Connect Card',
                  'type': 'to'
                }
              ],
            'autotext': 'true',
            'subject': staff+' ' +fname + ' ' + lname + ' Wants to Get Connected',
            'html':  staff+' ' +fname + ' ' + lname + ' Wants to Get Connected '+ staff+
            ' Here is their request: ' + request +
            ' Interest: ' + groups + ' ' + serving +
            ' Here is their contact info:' +
            ' Phone: ' + phone +
            ' Email: ' + email
          }
        }
    }).done(function(response)
        {
            if (response[0]['status'] == 'sent')
            {
                alert('Message Has Been Sent');
            }
            else
            {
                alert('Mesage could not be sent. Please try again.');
            }
        });
}